package eu.xenit.move2alf.repository.alfresco.server;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.ParameterDefinitionImpl;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;

public class EditAuditableAspectActionExecuter extends ActionExecuterAbstractBase {
	   private NodeService nodeService;
	   private BehaviourFilter behaviourFilter;

	@Override
	protected void executeImpl(Action action, NodeRef nodeRef) {
          Map<QName, Serializable> props = nodeService.getProperties(nodeRef);
          Serializable created = action.getParameterValue("created");
          Serializable creator = action.getParameterValue("creator");
          Serializable modified = action.getParameterValue("modified");
          Serializable modifier = action.getParameterValue("modifier");
          Serializable accessed = action.getParameterValue("accessed");
          if(created!=null)props.put(ContentModel.PROP_CREATED, created);
          if(creator!=null)props.put(ContentModel.PROP_CREATOR, creator);
          if(modified!=null)props.put(ContentModel.PROP_MODIFIED, modified);
          if(modifier!=null)props.put(ContentModel.PROP_MODIFIER, modifier);
          if(accessed!=null)props.put(ContentModel.PROP_ACCESSED, accessed);
          
          behaviourFilter.disableBehaviour(ContentModel.ASPECT_AUDITABLE);
          nodeService.setProperties(nodeRef, props);	
	}

	@Override
	protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
		// we allow that a subset of the properties is edited, so mandatory=false
		 paramList.add(
		         new ParameterDefinitionImpl(                       // Create a new parameter defintion to add to the list
		            "created",                              // The name used to identify the parameter
		            DataTypeDefinition.DATETIME,                       // The parameter value type
		            false,                                           // Indicates whether the parameter is mandatory
		            getParamDisplayLabel("Created")));      // The parameters display label
		 paramList.add(
		         new ParameterDefinitionImpl(
		            "creator",
		            DataTypeDefinition.TEXT,
		            false,
		            getParamDisplayLabel("Creator")));
		 paramList.add(
		         new ParameterDefinitionImpl(
		            "modified",
		            DataTypeDefinition.DATETIME,
		            false,
		            getParamDisplayLabel("Modified")));
		 paramList.add(
		         new ParameterDefinitionImpl(
		            "modifier",
		            DataTypeDefinition.TEXT,
		            false,
		            getParamDisplayLabel("Modifier")));
		 paramList.add(
		         new ParameterDefinitionImpl(
		            "accessed",
		            DataTypeDefinition.DATETIME,
		            false,
		            getParamDisplayLabel("Accessed")));
	}

	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	public void setBehaviourFilter(BehaviourFilter behaviourFilter) {
		this.behaviourFilter = behaviourFilter;
	}

}
